@extends('client.layout')

@section('title', 'Hỗ trợ viết Khóa luận/luận văn | Polish dissertation')

@section('content')

    <section class="landing" style="background-image: url('/images/student/studentpage.jpeg')">
        @include('client.header')

        <div class="landing-content">
            <div class="container">
                <div class="row align-items-center full-height">
                    <div class="col">
                        <h1 class="landing-title" style="font-size:30pt;font-family:'serif', serif, 20px;">Giúp bạn viết khóa luận/đồ án tốt nghiệp</h1>
                        <!-- <a class="landing-action" href="{{ route('survey.init') }}">Tính xác suất</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sub-landing">
        <div class="container">
            <h3 style="font-size:30pt;font-family:'Palatino Linotype', Tahoma, Times">Giúp bạn viết đồ án/luận văn theo văn phong học thuật.</h3> <br>
            <p class="section-description">Fun fact: Đồ án/khóa luận - Dissertation xuất phát từ tiếng Latin, có nghĩa là "tranh luận". Ở Việt Nam, bạn hầu như không có cơ hội tranh luận trước hội đồng</p>
            <div class="section-action">
                <a class="section-link" href="{{ route('blog.list') }}">Blog</a>
            </div>
        </div>
    </section>

    <section class="navigation">
        <div class="container">
            <h3 class="section-title">Khám phá cách viết Dissertation</h3>
            <p class="section-description">Phần blog của chúng tôi chia sẻ những kinh nghiệm, những điều cần biết khi viết đồ án/khóa luận tốt nghiệp.</p>
            <div class="section-navigation">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="blog-navigation" style="background-image: url('/images/student/f1.jpg')">
                            <div class="overlay">
                                <div class="row align-items-center navigation-block">
                                    <div class="col">
                                        <a href="/blog/61d3f4cb65ca2" class="navigation-title">Bố cục của một luận văn</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="student-navigation" style="background-image: url('/images/student/f2.jpg')">
                            <div class="overlay">
                                <div class="row align-items-center navigation-block">
                                    <div class="col">
                                        <a href="/blog/61d3f4cb65ca2" class="navigation-title">Cách viết bài báo<br> khoa học</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="tourism-navigation" style="background-image: url('/images/student/f3.jpg')">
                            <div class="overlay">
                                <div class="row align-items-center navigation-block">
                                    <div class="col">
                                        <a href="/blog/61d30043a9275" class="navigation-title">Nguyên tắc khi viết<br>báo cáo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('client.footer')

@endsection
