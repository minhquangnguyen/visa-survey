<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144169045-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144169045-1');
</script>

<nav class="navbar navbar-dark navbar-expand-lg" id="mainNavigation">
    <a class="navbar-brand" href="{{ route('root') }}"> <img src="/images/home/imgonline-com-ua-Negative-0VmMApcWiEGgxGod.png" style="width:120px;height:40px;"  alt="Girl in a jacket"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu"
        aria-controls="mainMenu" aria-expanded="false" aria-label="Main Menu">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="mainMenu">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ $currentPage === 'blog' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('blog.list') }}">Blog - Kinh nghiệm viết bài báo</a>
            </li>
            <li class="nav-item {{ $currentPage === 'service' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('service') }}">Dịch vụ</a>
            </li>
            <li class="nav-item {{ $currentPage === 'student' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('student') }}">Dành cho sinh viên</a>
            </li>
            <li class="nav-item {{ $currentPage === 'tourism' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('tourism') }}">Thạc sĩ và Tiến sĩ</a>
            </li>
        </ul>
    </div>
</nav>
