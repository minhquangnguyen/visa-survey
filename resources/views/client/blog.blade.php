@extends('client.layout')

@section('title', $blog->title . ' | Dịch vụ xin visa cao cấp USVisa')
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<link media="all" type="text/css" rel="stylesheet" href="http://tamlyhoctoipham.com/assets/frontend/css/responsive.min.css">
	<link media="all" type="text/css" rel="stylesheet" href="http://tamlyhoctoipham.com/assets/frontend/css/main-stylesheet.min.css">
	<link media="all" type="text/css" rel="stylesheet" href="http://tamlyhoctoipham.com/assets/frontend/css/custom.css">

	<style>
	.my_text
	{
		font-family:   Tahoma, sans-serif;
		font-size:      13px;
		font-weight:    normal;
	}
	
	</style>
@section('content')

    <section class="blog-landing" style="background-image: url('/images/blog/doctoral.jpg')">
        @include('client.header')

        <div class="landing-content">
            <div class="container">
                <div class="row align-items-center full-height">
                    <div class="col">
                        <h2 class="landing-title"> {{ $blog->title }} </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="content" style="background-color: rgb(219,219,219);"> <div class="wrapper"> <div class="content-wrapper">
<div class="composs-main-content composs-main-content-s-1">
<h5 class="blog-create-time"><font color="black" size="2"><i>{{ $blog->created_at->format('M d Y') }}</i></font></h5>
				<section class="blog-body" style="background-color: rgb(256,256,256);">
		        		<div class="container">
						<div class="blog-container">
                					<h2 class="blog-title"> {{ $blog->title }} </h2>
							<div class="composs-main-article-head">
										<p>
											<?php echo explode("|||", $blog->content)[0]; ?>
										</p>
										<div class="col-md-8">
											<div class="blog-featured-image"  align="center" valign="center" style="background-image: url('{{ asset( $blog->image ) }}')"></div>
										</div>
										<div class="composs-main-article-meta">	</div>
										<div class="my_text"> {!! substr(explode("|||", $blog->content)[1], 5) !!} 
										</div>
							</div>
						</div>
					</div>
				</section>
</div>
				<aside id="sidebar">
<br>
							<div class="widget">
									<h3>Bài viết mới nhất</h3>
									<div class="widget-content ot-w-article-list" style="background-color: rgb(256,256,256);">
										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d1adcd06e985"><img src="http://usvisas.vn/upload/images/5d1adcd06e985_1562041552.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d1adcd06e985"><font color="black"> Tại sao nên cố gắng có visa trong lần đầu tiên nộp hồ sơ</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 13, 2019</span>
												</span>
											</div>
										</div>



										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d35a1085bcc9"><img src="http://usvisas.vn/upload/images/5d35a1085bcc9_1563795720.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d35a1085bcc9"><font color="black">9 hiểu lầm về quy trình xin visa Mỹ tại Việt Nam</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 23, 2019</span>
												</span>
											</div>
										</div>

										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d3697ca5dde1"><img src="http://usvisas.vn/upload/images/5d3697ca5dde1_1563858890.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d3697ca5dde1"><font color="black">Các bước xin visa không định cư</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 23, 2019</span>
												</span>
											</div>
										</div>


										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d36b4e2064e7"><img src="http://usvisas.vn/upload/images/5d36b4e2064e7_1563866338.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d36b4e2064e7"><font color="black">SEVIS và Chương trình sinh viên và trao đổi khách mời</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 23, 2019</span>
												</span>
											</div>
										</div>
										
										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d1afaac331de"><img src="http://usvisas.vn/upload/images/5d1afaac331de_1562049196.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d1afaac331de"><font color="black">Tại sao nên cho con cái sang Mỹ du học</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 13, 2019</span>
												</span>
											</div>
										</div>
										
										
										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d2712cdbe393"><img src="http://usvisas.vn/upload/images/5d2712cdbe393_1562841805.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d2712cdbe393"><font color="black">Hồ sơ visa trực tuyến DS160: những điều cần biết</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 13, 2019</span>
												</span>
											</div>
										</div>
											
										
										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d2713a62bbdd"><img src="http://usvisas.vn/upload/images/5d2713a62bbdd_1562842022.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d2713a62bbdd"><font color="black">Quy trình với visa sinh viên</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 13, 2019</span>
												</span>
											</div>
										</div>
										
										
										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d271e8665729"><img src="http://usvisas.vn/upload/images/5d271e8665729_1562844806.png" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d271e8665729"><font color="black">Một vài lưu ý về buổi phỏng vấn</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 13, 2019</span>
												</span>
											</div>
										</div>
										
										
										<div class="item">
											<div class="item-header">
												<a href="http://usvisas.vn/blog/5d27fa0b2b77b"><img src="http://usvisas.vn/upload/images/5d27fa0b2b77b_1562901003.jpeg" alt="" /></a>
											</div>
											<div class="item-content">
												<h4><a href="http://usvisas.vn/blog/5d27fa0b2b77b"><font color="black">Phỏng vấn: một số kĩ năng cần biết</font></a></h4>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons"></i>Jul 13, 2019</span>
												</span>
											</div>
										</div>
									</div>
							</div>
				</aside>
</div></div></div>
@include('client.footer')
@endsection
