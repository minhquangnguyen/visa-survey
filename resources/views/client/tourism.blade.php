@extends('client.layout')

@section('title', 'Papers | Viết báo cáo/đồ án tốt nghiệp')

@section('content')

    <section class="landing" style="background-image: url('/images/tourism/writing2.jpg')">
        @include('client.header')

        <div class="landing-content">
            <div class="container">
                <div class="row align-items-center full-height">
                    <div class="col">
                        <h2 style="font-size:30pt;font-family:'Palatino Linotype', Tahoma, Times" class="landing-title">Giúp bạn viết bài báo khoa học</h2>
                       <!--  <a class="landing-action" href="{{ route('survey.init') }}">Tính xác suất</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sub-landing">
        <div class="container">
            <h3 class="section-title" style="font-size:30pt;font-family:'Palatino Linotype', Tahoma, Times">...và đồ án tốt nghiệp của bạn</h3>
            <p class="section-description">Fun fact: Số người có học vị Tiến Sĩ nhỏ hơn 1% dân số thế giới. Tỷ lệ thất nghiệp trong số họ chỉ có 2.5%. </p>
			<p class="section-description">Thu nhập trung bình của PhD ở Mỹ là $1,624/tuần. Nếu tính cả sự nghiệp, một người có bằng PhD kiếm nhiều hơn người có bằng cử nhân 1.3 triệu $. </p>
            <div class="section-action">
                <a class="section-link" href="{{ route('blog.list') }}">Xem trang blog</a>
            </div>
        </div>
    </section>

    <section class="navigation">
        <div class="container">
            <h3 class="section-title" style="font-size:30pt;font-family:'Palatino Linotype', Tahoma, Times">Khám phá thông tin</h3>
            <p class="section-description">Phần blog của chúng tôi chia sẻ những kinh nghiệm, những điều cần biết khi viết papers.</p>
            <div class="section-navigation">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="blog-navigation" style="background-image: url('/images/home/_n1.jpg')">
                            <div class="overlay">
                                <div class="row align-items-center navigation-block">
                                    <div class="col">
                                        <a href="/blog/61d3f4cb65ca2" class="navigation-title">Cách viết luận văn tốt nghiệp</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="student-navigation" style="background-image: url('/images/home/_n2.jpg')">
                            <div class="overlay">
                                <div class="row align-items-center navigation-block">
                                    <div class="col">
                                        <a href="/blog/61ce8d5875d29" class="navigation-title">Viết bài báo khoa học</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="tourism-navigation" style="background-image: url('/images/home/_n3.jpg')">
                            <div class="overlay">
                                <div class="row align-items-center navigation-block">
                                    <div class="col">
                                        <a href="/blog/61d30043a9275" class="navigation-title">Một vài quy tắc <br>viết báo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('client.footer')

@endsection
