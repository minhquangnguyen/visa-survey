@extends('client.layout')
<style>
.button1 {
  background-color: #000000; 
  border-radius: 5px;
  color: #FFFFFF; 
  height: 45px;
}

.button1:hover {
  background-color: #FFFFFF;
  color: #000000;
}
</style>
@section('title', 'Dịch vụ | Scientific')
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<link media="all" type="text/css" rel="stylesheet" href="http://usvisas.vn/responsive.css">
	<link media="all" type="text/css" rel="stylesheet" href="http://usvisas.vn/style.css">

@section('content')
<section class="header" style="background-image: url('/images/blog/doctoral.jpg')">
        @include('client.header')
    </section>
<div class="twrapper" style="background-color:rgb(219, 219, 219);"> 
    <section class="pricing mt-5">
        <div class="container">
            <div class="row pt-5 mb-5">
<!--<div id="twrapper">-->
                <div class="col-sm-4 col-12" style="margin-bottom: 30px;">

                    <div class="card plan-block plan-basic text-center">
                        <div class="card-header">
                            <h4 class="mb-0 plan-title">Cơ bản</h4>
                        </div>
                        <div class="card-body" style="background-color: rgb(256,256,256);text-align:left">
                            <ul class="feature-list list-unstyled mb-4">
                                <li>1. Giúp bạn chỉnh sửa chính tả</li>
                                <li>2. Chỉnh sửa ngữ pháp</li>
                                <li>3. Góp ý về cấu trúc và tổ chức của bài báo</li>
                                <li>4. Tham vấn các tạp chí/hội nghị có thể nộp</li>
								<li>5. Không hoàn tiền</li>
                            </ul>
				<div style="text-align:center"><button type="button" class="button1" data-toggle="modal" data-target="#activePlan">Cơ bản</button></div>
                        </div>
                    </div>
                </div>
<br>
                <div class="col-sm-4 col-12" style="margin-bottom: 30px;">

                    <div class="card plan-block plan-advance text-center">
                        <div class="card-header">
                            <h4 class="mb-0 plan-title">Nâng cao</h4>
                        </div>
                        <div class="card-body" style="background-color: rgb(256,256,256);text-align:left">
                            <ul class="feature-list list-unstyled mb-4">
                                <li>1. Bạn có một sản phẩm nháp, trình bày bằng tiếng Việt</li>
                                <li>2. Chúng tôi giúp bạn viết lại bằng tiếng Anh theo văn phong khoa học</li>
                                <li>3. Gợi ý về các hội nghi/tạp chí có thể nộp.</li>
                                <li>4. Góp ý vể cấu trúc và tổ chức của bài báo.</li>
								<li>5. Thanh toán theo từng mức.</li>
                            </ul>
<div style="text-align:center"><button type="button" class="button1" data-toggle="modal" data-target="#activePlan">Nâng cao</button></div>
                        </div>
                    </div>
                </div>
<br>
                <div class="col-sm-4 col-12">

                    <div class="card plan-block plan-premium text-center">
                        <div class="card-header">
                            <h4 class="mb-0 plan-title">Cao Cấp</h4>
                        </div>
                        <div class="card-body" style="background-color: rgb(256,256,256);text-align:left">
                            <ul class="feature-list list-unstyled mb-4">
                                <li>1. Bạn có một ý tưởng</li>
                                <li>2. Chúng tôi giúp các bạn viết bài báo bằng tiếng Anh</li>
                                <li>3. Gợi ý về các hội nghi/tạp chí có thể nộp.</li>
                                <li>4. Góp ý vể cấu trúc và tổ chức của bài báo.</li>
                                <li>5. Thanh toán theo từng bước.</li>
                            </ul>
<div style="text-align:center"><button type="button" class="button1" style="text-align:center" data-toggle="modal" data-target="#activePlan">Cao cấp</button></div>
                        </div>
                    </div>
                </div>
<!--</div>-->
            </div>
        </div>
    </section>
</div>
    <section class="pricing mb-5">
        <div class="container">
            <div class="row pb-5">
                <p class="mb-0">Mọi thông tin khách hàng đều được bảo mật</p>
	    </div>
        </div>
    </section>
    <form action="{{ route('service.update') }}" method="post">
        @csrf

        <div class="modal fade plan-contact" id="activePlan" tabindex="-1" 
            role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Thông tin liên hệ</h5>
                    </div>
                    <div class="modal-body">
                        <p class="mb-4 mt-4">Hãy liên hệ với chúng tôi thông qua các cách sau đây:</p>
                        <p class="mb-4">
                            1. Liên lạc trực tiếp với chúng tôi qua SĐT: 
                            <span style="color: #56BF7B">0902287410</span>
                        </p>
                        <p class="mb-4">2. Để lại SĐT để chúng tôi liên hệ với bạn</p>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Đóng
                        </button>
                        <button type="submit" class="btn btn-primary">Lưu thông tin</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('client.footer')

@endsection
